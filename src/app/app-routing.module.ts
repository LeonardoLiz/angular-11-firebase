import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth/auth-guard.service';
import { ViewComponent } from './componentes/view/view.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {path: '', component: ViewComponent, canActivate: [AuthGuardService]},

  {path: 'main', loadChildren: './main/main.module#MainModule',  canActivate: [AuthGuardService]},
  {path: '**', component: NotFoundComponent , canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
