import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable, throwError } from 'rxjs';
import { User } from '../User.model';
import { catchError, switchMap, map } from 'rxjs/operators'
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userCollection: AngularFirestoreCollection<User> = this.afs.collection('users')

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private snackBar: MatSnackBar
  ) { }


  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }


  register(user: User): Observable<boolean>{
    return from(this.afAuth.createUserWithEmailAndPassword(user.email, user.password || ''))
    .pipe(
      switchMap((u: firebase.default.auth.UserCredential) => 
        this.userCollection.doc(u.user?.uid).set({...user, id: u.user?.uid}).then(()=>true)
      ),
      catchError((err)=> throwError(err))
    )
  }

  login(email: string, password: string): Observable<User | undefined>{
    return from(this.afAuth.signInWithEmailAndPassword(email, password))
    .pipe(
      switchMap((u: firebase.default.auth.UserCredential) => this.userCollection.doc<User>(u.user?.uid).valueChanges()),
      catchError(() => throwError('Invalid credentialls or user is not registred'))
    )
  }

  logout() {
    this.afAuth.signOut();
  }

  getUser(): Observable<boolean>{
    return this.afAuth.authState.pipe(
     map(u => (u) ? true : false)
    )
  }


}
