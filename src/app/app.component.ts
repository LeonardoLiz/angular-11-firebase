import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth/service/auth.service';
import { User } from './auth/User.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fire2';
  user$!: Observable<User>;
  authenticated$!: Observable<boolean>

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.authenticated$ = this.authService.getUser()
  }

  logout() {
    this.authService.logout()
    this.router.navigate([`/auth/login`])
  }
}
