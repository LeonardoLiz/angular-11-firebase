export interface Person {
    name: string
    email: string
    country: string
}