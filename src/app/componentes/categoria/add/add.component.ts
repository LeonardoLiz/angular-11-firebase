import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from '../../service/categoria.service';
import { categoria } from '../categoria.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  categoriaForm = this.fb.group({
    id: [undefined],
    titulo: ['', [Validators.required]]
  })

  constructor(
    private fb: FormBuilder,
    private categoriaService:  CategoriaService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let p: categoria = this.categoriaForm.value;
      this.addCat(p)
    }

  addCat(p: categoria){
    this.categoriaService.addCategoria(p)
      .then(() => {
        this.categoriaService.showMessage("Categoria Adicionada")
      })
      .catch(() => {
        this.categoriaService.showMessage("ERROR: Erro ao adcionar categoria!")
      })
  }



}
